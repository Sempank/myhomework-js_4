let menuBtn = document.getElementsByClassName('navbar-menu-block')[0];
menuBtn.onclick = () => {
    let menuIcon = document.getElementById('menu-icon');
    menuIcon.classList.toggle('fa-times');
    let navMenu = document.getElementsByClassName('navbar-menu')[0];
    navMenu.classList.toggle('active');

}
